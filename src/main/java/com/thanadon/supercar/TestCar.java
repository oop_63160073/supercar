/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.supercar;

/**
 *
 * @author Acer
 */
public class TestCar {

    public static void main(String[] args) {
        SuperCar car1 = new SuperCar("McLaren 765lt", 11800000, 765);
        car1.printCar();

        System.out.println();
        SuperCar car2 = new SuperCar("Maserati MC20", 17000000, 630);
        car2.printCar();

        System.out.println();
        car1.vsPrice(car2);

        System.out.println();
        car1.vsHP(car2);
        
        System.out.println();
        car2.vsPrice(car2);
    }
}

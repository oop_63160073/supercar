/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.supercar;

/**
 *
 * @author Acer
 */
public class SuperCar {

    private String name;
    private int price;
    private int horsepower;

    public SuperCar(String name, int price, int horsepower) {
        this.name = name;
        this.price = price;
        this.horsepower = horsepower;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void printCar() {
        System.out.println("Car: " + name + "\nPrice: " + price + " Baht\nHorsepower: " + horsepower + " hp");
    }

    public void vsPrice(SuperCar car) {
        if (this.price > car.getPrice()) {
            System.out.println(this.name + " price more than " + car.getName() + " = " + Math.abs(this.getPrice() - car.getPrice()) + " Baht");
        } else if (this.price < car.getPrice()) {
            System.out.println(this.name + " price less than " + car.getName() + " = " + Math.abs(this.getPrice() - car.getPrice()) + " Baht");
        } else {
            System.out.println(this.name + " price equal to " + car.getName());
        }
    }

    public void vsHP(SuperCar car) {
        if (this.horsepower > car.getHorsepower()) {
            System.out.println(this.name + " horsepower more than " + car.getName() + " = " + Math.abs(this.getHorsepower() - car.getHorsepower()) + " hp");
        } else if (this.horsepower < car.getHorsepower()) {
            System.out.println(this.name + " horsepower less than " + car.getName() + " = " + Math.abs(this.getHorsepower() - car.getHorsepower()) + " hp");
        } else {
            System.out.println(this.name + " horsepower equal to " + car.getName());
        }
    }
}
